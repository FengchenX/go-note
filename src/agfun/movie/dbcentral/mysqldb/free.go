package mysqldb

import (
	"agfun/movie/entity"
	"agfun/util"
	"fmt"
)

func AddFreeMovies(free *entity.FreeMovie) error {
	db := getSysDB().Create(free)
	return db.Error
}
func GetFreeMovies(free entity.FreeMovie, filter *util.PageFilter) ([]*entity.FreeMovie, int, error) {
	sql := ""
	var params []interface{}
	comma := ""
	if len(free.ID) > 0 {
		sql = fmt.Sprintf("%s %s id = ?", sql, comma)
		params = append(params, free.ID)
		comma = "AND"
	}
	if len(free.MovieID) > 0 {
		sql = fmt.Sprintf("%s %s movie_id = ?", sql, comma)
		params = append(params, free.MovieID)
		comma = "AND"
	}
	if len(free.FreeVideoID) > 0 {
		sql = fmt.Sprintf("%s %s free_video_id = ?", sql, comma)
		params = append(params, free.FreeVideoID)
		comma = "AND"
	}
	var frees []*entity.FreeMovie
	var total int
	db := getSysDB().Model(&entity.FreeMovie{}).Where(sql, params...).Count(&total)
	if db.Error != nil {
		return nil, -1, db.Error
	}
	sql = util.PageFilterSql(sql, "id", filter)
	db = getSysDB().Where(sql, params...).Find(&frees)
	return frees, total, db.Error
}

func UpdateFreeMovie(free entity.FreeMovie, args map[string]interface{}) error {
	up := make(map[string]interface{}, 20)
	if len(free.ID) > 0 {
		up["id"] = free.ID
	}
	if len(free.MovieID) > 0 {
		up["movie_id"] = free.MovieID
	}
	if len(free.FreeVideoID) > 0 {
		up["free_video_id"] = free.FreeVideoID
	}

	if len(free.ID) > 0 {
		db := getSysDB().Model(&free).Updates(up)
		return db.Error
	} else {
		sql := ""
		var params []interface{}
		comma := ""
		for k, v := range args {
			sql = fmt.Sprintf("%s %s %s =?", sql, comma, k)
			params = append(params, v)
			comma = "AND"
		}
		db := getSysDB().Model(&entity.FreeMovie{}).Updates(up).Where(sql, params...)
		return db.Error
	}
}

func DelFreeMovie(free entity.FreeMovie) error {
	if len(free.ID) > 0 {
		db := getSysDB().Delete(&free)
		return db.Error
	}
	sql := ""
	var params []interface{}
	comma := ""
	if len(free.ID) > 0 {
		sql = fmt.Sprintf("%s %s id = ?", sql, comma)
		params = append(params, free.ID)
		comma = "AND"
	}
	//if len(free.Name) > 0 {
	//	sql = fmt.Sprintf("%s %s name = ?", sql, comma)
	//	params = append(params, free.Name)
	//	comma = "AND"
	//}
	db := getSysDB().Delete(&entity.FreeMovie{}).Where(sql, params...)
	return db.Error
}
